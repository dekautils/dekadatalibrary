#include "format/format.hpp"
#include <sstream>
#include <stdexcept>

namespace DekaDataLibrary {
namespace Format {

    Format::Format()
        : Format(
            String("Invalid"), []() -> ISerializerPtr { return nullptr; }, []() -> IDeserializerPtr { return nullptr; })
    {
    }

    Format::Format(String formatName, SerializerCreatorMethod newSerializerMethod, DeserializerCreatorMethod newDeserializerMethod)
    {
        name = std::move(formatName);
        serializerMethod = std::move(newSerializerMethod);
        deserializerMethod = std::move(newDeserializerMethod);
    }

    String Format::getName() const
    {
        return name;
    }

    ISerializerPtr Format::getSerializer() const
    {
        return serializerMethod();
    }

    IDeserializerPtr Format::getDeserializer() const
    {
        return deserializerMethod();
    }

    String Format::serializeToString(const Value& val)
    {
        auto out_stringStream = std::basic_ostringstream<String::WChar>();
        ISerializerPtr serializer = getSerializer();

        serializer->setOutputStream(&out_stringStream);
        if (!serializer->serialize(val)) {
            std::string errMsg = "Cannot serialize to string with format ";
            errMsg += getName().tostr() + ": ";
            errMsg += serializer->getStatusMsg().tostr();
            throw std::runtime_error(errMsg);
        }

        return out_stringStream.str();
    }

    Value Format::deserializeFromString(const String& str)
    {
        auto in_stringstream = std::basic_istringstream<String::WChar>(str);
        IDeserializerPtr deserializer = getDeserializer();
        deserializer->setInputStream(&in_stringstream);

        Value out;
        if (!deserializer->deserialize(out)) {
            std::string errMsg = "Cannot deserialize from string with format ";
            errMsg += getName().tostr() + ": ";
            errMsg += deserializer->getStatusMsg().tostr();
            throw std::runtime_error(errMsg);
        }

        return out;
    }

}
}
