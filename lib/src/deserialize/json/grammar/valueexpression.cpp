#include "deserialize/json/grammar/valueexpression.hpp"
#include "deserialize/json/grammar/arrayexpression.hpp"
#include "deserialize/json/grammar/objectexpression.hpp"
#include <cassert>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value ValueExpression::parse(JSONParseContextPtr ctx)
    {
        auto token = ctx->peekNextToken();

        if (token.getType() == TokenTypes::String
            || token.getType() == TokenTypes::Bool
            || token.getType() == TokenTypes::Null
            || token.getType() == TokenTypes::Number) {

            //assert(ctx->readNextToken().getType() != TokenTypes::ObjectBegin);
            ctx->readNextToken();
            return token.getVal();

        } else if (token.getType() == TokenTypes::ObjectBegin) {

            ObjectExpression parser;
            return parser.parse(ctx);

        } else if (token.getType() == TokenTypes::ArrayBegin) {

            ArrayExpression parser;
            return parser.parse(ctx);
        }

        throw std::runtime_error("Internal problem: unsupported type");
    }

}
}
