#include "serialize/iserializer.hpp"

namespace DekaDataLibrary {
namespace Serialize {

    void ISerializer::setOutputStream(SerializerOutputStream* new_stream)
    {
        this->stream = new_stream;
    }

}
}
