#ifndef TEMPLATEDFORMAT_HPP
#define TEMPLATEDFORMAT_HPP

#include "../deserialize/ideserializer.hpp"
#include "../serialize/iserializer.hpp"
#include "format.hpp"
#include <type_traits>

namespace DekaDataLibrary {
namespace Format {

    template <class SerializerType, class DeserializerType>
    class TemplatedFormat : public Format {
        static_assert(std::is_base_of<DekaDataLibrary::Serialize::ISerializer, SerializerType>::value, "Serializer type must be derived from DekaDataLibrary::Serialize::ISerializer");
        static_assert(std::is_base_of<DekaDataLibrary::Deserialize::IDeserializer, DeserializerType>::value, "Deserializer type must be derived from DekaDataLibrary::Deserialize::IDeserializer");

    public:
        TemplatedFormat(const char* formatName = "Unspecified");
    };

    template <class SerializerType, class DeserializerType>
    TemplatedFormat<SerializerType, DeserializerType>::TemplatedFormat(const char* formatName)
        : Format(
            formatName,
            []() -> ISerializerPtr { return std::make_shared<SerializerType>(); },
            []() -> IDeserializerPtr { return std::make_shared<DeserializerType>(); })
    {
    }

}
}

#endif // TEMPLATEDFORMAT_HPP
