#ifndef DEKADATALIBRARY_HPP
#define DEKADATALIBRARY_HPP

#include "array.hpp"
#include "deserialize/ideserializer.hpp"
#include "format/format.hpp"
#include "format/templatedformat.hpp"
#include "object.hpp"
#include "serialize/iserializer.hpp"
#include "string.hpp"
#include "value.hpp"
#include "valuetype.hpp"

//Property
#include "deserialize/property/propertiesdeserializer.hpp"
#include "serialize/property/propertiesserializer.hpp"

//JSON
#include "deserialize/json/jsondeserializer.hpp"
#include "serialize/json/jsonserializer.hpp"

#endif // DEKADATALIBRARY_HPP
