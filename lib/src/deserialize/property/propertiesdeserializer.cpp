#include "deserialize/property/propertiesdeserializer.hpp"
#include <sstream>

namespace DekaDataLibrary::Deserialize::Property {

PropertiesDeserializer::PropertiesDeserializer()
    : status("")
{
}

bool PropertiesDeserializer::deserialize(Value& valueToFill)
{
    if (!stream) {
        status = "Invalid stream";
        return false;
    }

    std::basic_stringbuf<String::WChar> buffer = std::basic_stringbuf<String::WChar>();

    Object outObj;

    while (stream->good()) {
        stream->get(buffer, '\n');
        stream->get(); //Extract new line

        String line = buffer.str();
        String key = "";
        String value = "";

        auto code = processLine(line, key, value);

        switch (code) {
        case ProcessLineResult::Error:
            return false;
        case ProcessLineResult::Ok:
            outObj[key] = String(value);
            break;
        case ProcessLineResult::Ignore:
            break;
        }
        buffer = std::basic_stringbuf<String::WChar>();
    }

    valueToFill = outObj;
    return true;
}

String PropertiesDeserializer::getStatusMsg() const
{
    return status;
}

ValueTypes PropertiesDeserializer::getSupportedTypes() const
{
    return { ValueType::String, ValueType::Object };
}

PropertiesDeserializer::ProcessLineResult PropertiesDeserializer::processLine(const String& line, String& key, String& value)
{
    String in = line.simplified();

    if (in.size() == 0) {
        return ProcessLineResult::Ignore;
    }

    if (in[0] == '#') {
        return ProcessLineResult::Ignore;
    }

    auto posEqual = in.find('=');
    auto posComment = in.find('#');

    if (posEqual == String::StringType::npos) {
        status = L"On the line '" + line + L"' no assigning";
        return ProcessLineResult::Error;
    }

    if (posComment != String::StringType::npos) {
        if (posComment <= posEqual) {
            status = L"Comment before assigning on the line: " + line;
            return ProcessLineResult::Error;
        } else {
            in = in.erase(posComment);
            in = in.simplified();
        }
    }

    String key_from_line = in.substr(0, posEqual);
    in = in.erase(0, posEqual + 1);
    String value_from_line = in;

    key = key_from_line;
    value = value_from_line;

    return ProcessLineResult::Ok;
}

}
