#ifndef DDL_OBJECTEXPRESSION_HPP
#define DDL_OBJECTEXPRESSION_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT ObjectExpression : public AbstractJSONExpression {
    public:
        ObjectExpression() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}
#endif // DDL_OBJECTEXPRESSION_HPP
