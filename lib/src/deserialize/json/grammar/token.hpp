#ifndef DDL_TOKEN_HPP
#define DDL_TOKEN_HPP

#include "../../../value.hpp"
#include "dekadatalibrary_export.hpp"
#include "tokentypes.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT Token {
    private:
        TokenTypes type;
        Value val;

    public:
        Token(TokenTypes new_type = TokenTypes::Undefined, Value new_val = Value());

        TokenTypes getType() const;
        Value getVal() const;
    };

}
}

#endif // DDL_TOKEN_HPP
