#ifndef DDL_OBJECT_HPP
#define DDL_OBJECT_HPP

#include "dekadatalibrary_export.hpp"
#include "valuetype.hpp"
#include <map>

namespace DekaDataLibrary {

class Value;

/**
 * @brief The Object class represents object with attributes. It's usage like as map from std library.
 */
class DEKADATALIBRARY_EXPORT Object : public std::map<String, Value> {
};

}
#endif // DDL_OBJECT_HPP
