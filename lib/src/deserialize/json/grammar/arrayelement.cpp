#include "deserialize/json/grammar/arrayelement.hpp"
#include "deserialize/json/grammar/valueexpression.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value ArrayElement::parse(JSONParseContextPtr ctx)
    {
        ValueExpression exp;
        Value val = exp.parse(ctx);
        Array arr;
        arr.push_back(val);
        return arr;
    }

}
}
