#ifndef DDL_JSONEXPRESSION_HPP
#define DDL_JSONEXPRESSION_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT JsonExpression : public AbstractJSONExpression {
    public:
        JsonExpression() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}

#endif // DDL_JSONEXPRESSION_HPP
