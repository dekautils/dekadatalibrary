#ifndef DDL_FORMAT_HPP
#define DDL_FORMAT_HPP

#include "../deserialize/ideserializer.hpp"
#include "../serialize/iserializer.hpp"
#include "../valuetype.hpp"
#include "dekadatalibrary_export.hpp"
#include <functional>
#include <memory>

namespace DekaDataLibrary {
namespace Format {

    using ISerializerPtr = std::shared_ptr<DekaDataLibrary::Serialize::ISerializer>;
    using SerializerCreatorMethod = std::function<ISerializerPtr()>;

    using IDeserializerPtr = std::shared_ptr<DekaDataLibrary::Deserialize::IDeserializer>;
    using DeserializerCreatorMethod = std::function<IDeserializerPtr()>;

    /**
     * @brief The Format class represent format with serializer and deserializer
     */
    class DEKADATALIBRARY_EXPORT Format {
    private:
        String name; ///< Format name
        SerializerCreatorMethod serializerMethod; ///< Creator method for ISerializer
        DeserializerCreatorMethod deserializerMethod; ///< Creator method for IDeserializer

    public:
        /**
         * @brief Format creates Mock Format
         */
        Format();
        /**
         * @brief Format creates format
         * @param formatName name of format
         * @param newSerializerMethod creator method for ISerializer
         * @param newDeserializerMethod creator method for IDeserializer
         */
        Format(String formatName, SerializerCreatorMethod newSerializerMethod, DeserializerCreatorMethod newDeserializerMethod);

        String getName() const; ///< Returns format name
        ISerializerPtr getSerializer() const; ///< Returns ISerializer
        IDeserializerPtr getDeserializer() const; ///< Returns IDeserializer

        /**
         * @brief serializeToString serializes to string using this format
         * @param val value to serialize
         * @return string with serialized value
         * @remark uses getSerializer to create instance
         */
        virtual String serializeToString(const Value& val);
        /**
         * @brief deserializeFromString deserialize string using this format
         * @param str string to deserialize
         * @return Value object with data from string
         * @remark uses getDeserializer to create instance
         */
        virtual Value deserializeFromString(const String& str);
    };

}
}

#endif // DDL_FORMAT_HPP
