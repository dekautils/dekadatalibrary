#include "deserialize/property/propertiesdeserializer.hpp"
#include "valuetype.hpp"
#include <gtest/gtest.h>
#include <sstream>

using namespace DekaDataLibrary::Deserialize::Property;
using namespace DekaDataLibrary::Deserialize;
using namespace DekaDataLibrary;
using namespace testing;

class TestPropertiesDeserializer : public Test {
protected:
    PropertiesDeserializer deserializer;

    // Test interface
protected:
    void SetUp() override
    {
        deserializer = PropertiesDeserializer();
    }
};

TEST_F(TestPropertiesDeserializer, objectDeserializeValid)
{
    String objStr = "name=test\n"
                    "op=plus\n"
                    "value=x y z\n";

    std::basic_istringstream<String::WChar> stream(objStr);
    deserializer.setInputStream(&stream);

    Value value;
    ASSERT_TRUE(deserializer.deserialize(value));

    ASSERT_TRUE(value.isObject());
    Object obj = value;
    ASSERT_EQ(obj.size(), 3);
    for (auto pair : obj) {
        ASSERT_TRUE(pair.second.isString());
    }

    ASSERT_EQ(obj["name"], String("test"));
    ASSERT_EQ(obj["op"], String("plus"));
    ASSERT_EQ(obj["value"], String("x y z"));
}

TEST_F(TestPropertiesDeserializer, objectDeserializeComment)
{
    String objStr = "name=test\n"
                    "#op=plus\n"
                    "value=x y z\n";

    std::basic_istringstream<String::WChar> stream(objStr);
    deserializer.setInputStream(&stream);

    Value value;
    ASSERT_TRUE(deserializer.deserialize(value));

    ASSERT_TRUE(value.isObject());
    Object obj = value;
    ASSERT_EQ(obj.size(), 2);
    for (auto pair : obj) {
        ASSERT_TRUE(pair.second.isString());
    }

    ASSERT_EQ(obj["name"], String("test"));
    ASSERT_EQ(obj["value"], String("x y z"));
    ASSERT_ANY_THROW(auto a = obj.at("op"));
}

TEST_F(TestPropertiesDeserializer, objectDeserializeCommentAfterEqual)
{
    String objStr = "name=test\n"
                    "op=pl #us\n"
                    "value=x y z\n";

    std::basic_istringstream<String::WChar> stream(objStr);
    deserializer.setInputStream(&stream);

    Value value;
    ASSERT_TRUE(deserializer.deserialize(value));

    ASSERT_TRUE(value.isObject());
    Object obj = value;
    ASSERT_EQ(obj.size(), 3);
    for (auto pair : obj) {
        ASSERT_TRUE(pair.second.isString());
    }

    ASSERT_EQ(obj["name"], String("test"));
    ASSERT_EQ(obj["op"], String("pl"));
    ASSERT_EQ(obj["value"], String("x y z"));
}

TEST_F(TestPropertiesDeserializer, objectDeserializeFailCommentBeforeEqual)
{
    String objStr = "name=test\n"
                    "o#p=plus\n"
                    "value=x y z\n";

    std::basic_istringstream<String::WChar> stream(objStr);
    deserializer.setInputStream(&stream);

    Value value;
    ASSERT_FALSE(deserializer.deserialize(value));
}

TEST_F(TestPropertiesDeserializer, objectDeserializeFailNoStream)
{
    deserializer.setInputStream(nullptr);

    Value value;
    ASSERT_FALSE(deserializer.deserialize(value));
}

TEST_F(TestPropertiesDeserializer, objectDeserializeFailNoAssign)
{
    String objStr = "name=test\n"
                    "opplus\n"
                    "value=x y z\n";

    std::basic_istringstream<String::WChar> stream(objStr);
    deserializer.setInputStream(&stream);

    Value value;
    ASSERT_FALSE(deserializer.deserialize(value));
}
