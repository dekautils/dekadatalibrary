#ifndef DDL_ARRAYELEMENT_HPP
#define DDL_ARRAYELEMENT_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT ArrayElement : public AbstractJSONExpression {
    public:
        ArrayElement() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}

#endif // DDL_ARRAYELEMENT_HPP
