#include "serialize/property/propertiesserializer.hpp"
#include <algorithm>
#include <sstream>

namespace DekaDataLibrary::Serialize::Property {

PropertiesSerializer::PropertiesSerializer()
    : status("")
{
}

bool PropertiesSerializer::serialize(const Value& val)
{
    if (!stream) {
        status = "Stream is null";
        return false;
    }

    if (!val.isObject()) {
        status = "Root element must be Object type";
        return false;
    }

    Object obj = val.toObject();

    auto tmp_stream = std::basic_ostringstream<String::WChar>(std::ios_base::out);

    for (auto pair : obj) {
        auto key = pair.first;
        auto value = pair.second;

        auto pos = key.find('=');
        if (pos != String::StringType::npos) {
            status = "Cannot write key '" + key.tostr() + "' it has unsupported char '='";
            return false;
        }

        pos = key.find('#');
        if (pos != String::StringType::npos) {
            status = "Cannot write key '" + key.tostr() + "' it has unsupported char '#'";
            return false;
        }

        if (!value.isString()) {
            status = "Cannot write object attrubute '" + key.tostr() + "' it has unsupported type";
            return false;
        }

        key = key.simplified();
        auto valueStr = value.toString().simplified();

        pos = valueStr.find('#');
        if (pos != String::StringType::npos) {
            status = "Cannot write value '" + valueStr.tostr() + "' it has unsupported char '#'";
            return false;
        }

        tmp_stream << key.towstr() << " = " << valueStr.towstr() << std::endl;
    }

    *stream << tmp_stream.str();
    return true;
}

String PropertiesSerializer::getStatusMsg() const
{
    return status;
}

ValueTypes PropertiesSerializer::getSupportedTypes() const
{
    return { ValueType::Object, ValueType::String };
}

}
