#include "serialize/json/jsonserializer.hpp"
#include <iomanip>

namespace DekaDataLibrary::Serialize::JSON {

void JSONSerializer::setFloatPointPrecision(int value)
{
    floatPointPrecision = value;
}

JSONSerializer::JSONSerializer()
    : status("")
    , floatPointPrecision(8)
{
}

bool JSONSerializer::serialize(const Value& val)
{
    if (!stream) {
        status = "Invalid output stream";
        return false;
    }

    tmp_stream = std::basic_ostringstream<String::WChar>(std::ios_base::out);

    serializeValue(val);

    *stream << tmp_stream.str();
    return true;
}

String JSONSerializer::getStatusMsg() const
{
    return status;
}

ValueTypes JSONSerializer::getSupportedTypes() const
{
    return { ValueType::Int, ValueType::Float, ValueType::Bool, ValueType::String, ValueType::Null, ValueType::Array, ValueType::Object };
}

bool JSONSerializer::serializeValue(const Value& val)
{
    auto type = val.getType();

    switch (type) {

    case ValueType::Int:
        tmp_stream << val.toInt();
        break;

    case ValueType::Float:
        if (!serializeFloat(val.toFloat()))
            return false;
        break;

    case ValueType::Bool:
        tmp_stream << ((val.toBool()) ? L"true" : L"false");
        break;

    case ValueType::String:
        if (!serializeString(val.toString()))
            return false;
        break;

    case ValueType::Null:
        tmp_stream << "null";
        break;

    case ValueType::Array:
        if (!serializeArray(val.toArray()))
            return false;
        break;

    case ValueType::Object:
        if (!serializeObject(val.toObject()))
            return false;
        break;

    case ValueType::Undefined:
        status = "Unsupported type detected : Undefined";
        return false;
    }

    return true;
}

bool JSONSerializer::serializeObject(const Object& obj)
{
    tmp_stream << "{ ";

    size_t attributesCount = obj.size();
    size_t counter = 1;
    for (const auto& pair : obj) {
        auto key = pair.first;
        auto val = pair.second;

        //Write "key": value
        if (!serializeString(key))
            return false;
        tmp_stream << " : ";
        if (!serializeValue(val))
            return false;

        //Write , if needed
        if (counter < attributesCount) {
            tmp_stream << ", ";
        }

        counter++;
    }

    tmp_stream << "}";
    return true;
}

bool JSONSerializer::serializeArray(const Array& arr)
{
    tmp_stream << "[ ";

    size_t elementsCount = arr.size();
    size_t counter = 1;
    for (const auto& val : arr) {

        if (!serializeValue(val))
            return false;
        if (counter < elementsCount) {
            tmp_stream << ", ";
        }

        counter++;
    }

    tmp_stream << "]";
    return true;
}

bool JSONSerializer::serializeFloat(const FloatType& val)
{
    tmp_stream << std::setprecision(floatPointPrecision + 1) << val;
    return true;
}

bool JSONSerializer::serializeString(const String& str)
{
    auto preprocessedStr = str;
    std::list<String> controlChars = { "\"", "\\", "\b", "\f", "\n", "\r", "\t" };

    for (auto controlStr : controlChars) {
        preprocessedStr.replaceAll(controlStr, L"\\" + controlStr);
    }

    tmp_stream << "\"";
    tmp_stream << preprocessedStr;
    tmp_stream << "\"";
    return true;
}

}
