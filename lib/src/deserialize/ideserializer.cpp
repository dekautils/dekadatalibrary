#include "deserialize/ideserializer.hpp"

namespace DekaDataLibrary {
namespace Deserialize {

    void IDeserializer::setInputStream(DeserializeInputStream* new_stream)
    {
        this->stream = new_stream;
    }

}
}
