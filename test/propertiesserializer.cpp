#include "serialize/property/propertiesserializer.hpp"
#include "valuetype.hpp"
#include <gtest/gtest.h>
#include <sstream>

using namespace DekaDataLibrary::Serialize::Property;
using namespace DekaDataLibrary::Serialize;
using namespace DekaDataLibrary;
using namespace testing;

class TestPropertiesSerializer : public Test {
protected:
    PropertiesSerializer serializer;

    // Test interface
protected:
    void SetUp() override
    {
        serializer = PropertiesSerializer();
    }
};

TEST_F(TestPropertiesSerializer, ValidSupportedTypes)
{
    auto list = serializer.getSupportedTypes();
    ASSERT_EQ(list.size(), 2);

    bool haveObjectType = std::find(list.begin(), list.end(), ValueType::Object) != list.end();
    bool haveStringType = std::find(list.begin(), list.end(), ValueType::String) != list.end();

    ASSERT_TRUE(haveObjectType && haveStringType);
}

TEST_F(TestPropertiesSerializer, failIfNoStream)
{
    Object obj;
    obj["test"] = String("data");
    serializer.setOutputStream(nullptr);
    ASSERT_FALSE(serializer.serialize(obj));
}

TEST_F(TestPropertiesSerializer, failIfEqualInKey)
{
    Object obj;
    std::basic_ostringstream<String::WChar> stream(std::ios_base::out);
    obj["te=st"] = String("data");
    serializer.setOutputStream(&stream);
    ASSERT_FALSE(serializer.serialize(obj));
}

TEST_F(TestPropertiesSerializer, failIfRootNotValue)
{
    std::basic_ostringstream<String::WChar> stream(std::ios_base::out);
    serializer.setOutputStream(&stream);
    ASSERT_FALSE(serializer.serialize(5));
}

TEST_F(TestPropertiesSerializer, failIfObjectValueNotString)
{
    Object obj;
    std::basic_ostringstream<String::WChar> stream(std::ios_base::out);
    obj["test"] = 5;
    serializer.setOutputStream(&stream);
    ASSERT_FALSE(serializer.serialize(obj));
}

TEST_F(TestPropertiesSerializer, failIfKeyHaveCommentChar)
{
    Object obj;
    std::basic_ostringstream<String::WChar> stream(std::ios_base::out);
    obj["t#est"] = String("data");
    serializer.setOutputStream(&stream);
    ASSERT_FALSE(serializer.serialize(obj));
}

TEST_F(TestPropertiesSerializer, failIfObjectValueHaveCommentChar)
{
    Object obj;
    std::basic_ostringstream<String::WChar> stream(std::ios_base::out);
    obj["test"] = String("dat#a");
    serializer.setOutputStream(&stream);
    ASSERT_FALSE(serializer.serialize(obj));
}

TEST_F(TestPropertiesSerializer, serializing)
{
    Object obj;
    std::basic_ostringstream<String::WChar> stream(std::ios_base::out);
    obj["att"] = String("none object");
    obj["test"] = String("data not avaiable");

    //Object sorts attributes
    String expected = "att = none object\n"
                      "test = data not avaiable\n";

    serializer.setOutputStream(&stream);
    ASSERT_TRUE(serializer.serialize(obj));

    String serializedStr = stream.str();

    ASSERT_EQ(serializedStr, expected);
}
