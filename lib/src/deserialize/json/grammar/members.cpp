#include "deserialize/json/grammar/members.hpp"
#include "deserialize/json/grammar/member.hpp"
#include <cassert>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value Members::parse(JSONParseContextPtr ctx)
    {
        Member elementParser;
        Value el = elementParser.parse(ctx);
        assert(el.isObject());

        auto delim = ctx->peekNextToken();
        if (delim.getType() != TokenTypes::Comma) {
            return el;
        }

        //assert(ctx->readNextToken().getType() == TokenTypes::Comma);
        ctx->readNextToken();

        Members elementsParser;
        Value objectElements = elementsParser.parse(ctx);
        if (!objectElements.isObject()) {
            throw std::runtime_error("Unexpected output from Members::parse : Not Object");
        }

        Object obj;
        for (auto e : el.toObject()) {
            obj[e.first] = e.second;
        }
        for (auto e : objectElements.toObject()) {
            obj[e.first] = e.second;
        }

        return obj;
    }

}
}
