#include "deserialize/json/jsondeserializer.hpp"
#include "deserialize/json/grammar/jsonexpression.hpp"
#include "deserialize/json/grammar/jsonparsecontext.hpp"
#include <stdexcept>

using namespace DekaDataLibrary::Deserialize::JSON::Grammar;

namespace DekaDataLibrary::Deserialize {
namespace JSON {

    JSONDeserializer::JSONDeserializer()
        : status("")
    {
    }

    bool JSONDeserializer::deserialize(Value& valueToFill)
    {
        if (!stream) {
            status = "Invalid stream";
            return false;
        }

        auto ctx = std::make_shared<JSONParseContext>(stream);
        JsonExpression parser;

        try {
            auto val = parser.parse(ctx);
            valueToFill = val;
            return true;
        } catch (const std::exception& ex) {
            status = std::string("Exception have caught: ") + ex.what();
        }

        return false;
    }

    String JSONDeserializer::getStatusMsg() const
    {
        return status;
    }

    ValueTypes JSONDeserializer::getSupportedTypes() const
    {
        using V = ValueType;
        return { V::Int, V::Bool, V::Null, V::Array, V::Float, V::Object, V::String };
    }

}
}
