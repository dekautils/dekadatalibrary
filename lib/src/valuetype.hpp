#ifndef DDL_VALUETYPE_HPP
#define DDL_VALUETYPE_HPP

#include "dekadatalibrary_export.hpp"
#include "string.hpp"
#include <list>
#include <string>

namespace DekaDataLibrary {

/**
 * @brief The ValueType enum defines value types
 */
enum class DEKADATALIBRARY_EXPORT ValueType {
    Undefined = 0, ///< Undefined
    Object, ///< Object type
    Array, ///< Array type
    Int, ///< Inty type
    Float, ///< Float type
    Bool, ///< True/False
    String, ///< Wide String
    Null ///< Null
};

using IntType = int;
using FloatType = double;
using BoolType = bool;

using ValueTypes = std::list<ValueType>;

}

#endif // DDL_VALUETYPE_HPP
