#include "deserialize/json/grammar/jsonparsecontext.hpp"
#include "deserialize/json/grammar/token.hpp"
#include <algorithm>
#include <cassert>
#include <sstream>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    size_t JSONParseContext::getLine() const
    {
        return line;
    }

    bool JSONParseContext::atEOF()
    {
        skipWhitespace();
        return stream->eof();
    }

    JSONParseContext::JSONParseContext(DeserializeInputStream* new_stream)
        : stream(new_stream)
    {
        if (!stream) {
            throw std::invalid_argument("Stream for JSONParseContext cannot be null");
        }

        line = 1;
        stream->seekg(0);
    }

    Token JSONParseContext::readNextToken()
    {
        if (!tokenStack.empty()) {
            auto token = tokenStack.top();
            tokenStack.pop();
            return token;
        }

        skipWhitespace();

        if (atEOF())
            throw getRuntime("Unexpected EOF");

        auto ch = stream->peek();

        Token out = { TokenTypes::Undefined, "" };

        switch (ch) {

        case L'{':
            //assert(stream->get() == L'{');
            stream->get();
            out = { TokenTypes::ObjectBegin, "{" };
            break;
        case L'}':
            //assert(stream->get() == L'}');
            stream->get();
            out = { TokenTypes::ObjectEnd, "}" };
            break;
        case L'[':
            //assert(stream->get() == L'[');
            stream->get();
            out = { TokenTypes::ArrayBegin, "[" };
            break;
        case L']':
            //assert(stream->get() == L']');
            stream->get();
            out = { TokenTypes::ArrayEnd, "]" };
            break;
        case L',':
            //assert(stream->get() == L',');
            stream->get();
            out = { TokenTypes::Comma, "," };
            break;
        case L':':
            //assert(stream->get() == L':');
            stream->get();
            out = { TokenTypes::Colon, ":" };
            break;

        case L't':
            out = readBool();
            break;
        case L'f':
            out = readBool();
            break;

        case L'n':
            out = readNull();
            break;

        case L'"':
            out = readString();
            break;

        default:
            out = readNumber();
            break;
        }

        return out;
    }

    Token JSONParseContext::peekNextToken()
    {
        if (!tokenStack.empty()) {
            auto token = tokenStack.top();
            return token;
        }

        auto token = readNextToken();
        tokenStack.push(token);
        return token;
    }

    void JSONParseContext::skipWhitespace()
    {
        if (stream->eof())
            return;

        auto ch = stream->peek();
        while (iswspace(ch)) {
            stream->get();
            //assert(iswspace(stream->get()));
            if (ch == '\n')
                line++;
            ch = stream->peek();
        }
    }

    Token JSONParseContext::readBool()
    {
        BoolType boolValue;
        *stream >> std::boolalpha >> boolValue;
        if (stream->fail()) {
            throw getRuntime("Unexpected bool token");
        }

        return { TokenTypes::Bool, boolValue };
    }

    Token JSONParseContext::readNull()
    {
        String readed;
        readed.resize(4, '\0');
        stream->get(readed.data(), 5);
        if (readed == L"null") {
            return { TokenTypes::Null, nullptr };
        } else {
            throw getRuntime("Unexpected token: " + readed.tostr());
        }
    }

    Token JSONParseContext::readNumber()
    {
        auto validationFunc = [](String::WChar ch) -> bool {
            if (iswdigit(ch))
                return true;
            else if (ch == L'.')
                return true;
            else if (ch == L'-' || ch == L'+')
                return true;
            else if (ch == L'e' || ch == L'E')
                return true;
            return false;
        };

        String buffer = L"";
        auto peeked = stream->peek();
        while (validationFunc(peeked) && !stream->eof()) {
            buffer += peeked;
            stream->get();
            //assert(peeked == stream->get());
            peeked = stream->peek();
        }

        bool isFloatNumber = false;
        std::list<String::WChar> floatSigns = { L'.', L'e', L'E' };
        for (auto floatSign : floatSigns) {
            if (std::find(buffer.begin(), buffer.end(), floatSign) != buffer.end()) {
                isFloatNumber = true;
                break;
            }
        }

        auto string_stream = std::basic_istringstream<String::WChar>(buffer);

        if (isFloatNumber) {
            FloatType floatReaded;
            string_stream >> floatReaded;
            if (stream->fail()) {
                throw getRuntime("Unexpected token");
            }

            return { TokenTypes::Number, floatReaded };

        } else {
            IntType intReaded;
            string_stream >> intReaded;
            if (stream->fail()) {
                throw getRuntime("Unexpected token");
            }

            return { TokenTypes::Number, intReaded };
        }
    }

    Token JSONParseContext::readString()
    {
        stream->get();
        //assert(stream->get() == L'"');

        String outStr = "";

        auto ch = stream->get();
        if (stream->eof())
            throw getRuntime("Unexpected EOF");
        while (ch != L'"') {
            if (ch == L'\\') {
                auto escapeChar = readEscapeChar();
                outStr += escapeChar;
            } else {
                outStr += ch;
            }

            ch = stream->get();
            if (ch != L'"' && stream->eof())
                throw getRuntime("Unexpected EOF");
        }

        return { TokenTypes::String, outStr };
    }

    String::WChar JSONParseContext::readEscapeChar()
    {
        if (atEOF())
            throw getRuntime("Unexpected EOF after escape char");
        //Escape char(\) consumed already
        auto ch = stream->get();
        switch (ch) {
        case '"':
            return '"';
        case '\\':
            return '\\';
        case '/':
            return '/';
        case 'b':
            return '\b';
        case 'f':
            return '\f';
        case 'n':
            return '\n';
        case 'r':
            return '\r';
        case 't':
            return '\t';
        default:
            throw getRuntime("Unexpected escape char");
        }
    }

    std::runtime_error JSONParseContext::getRuntime(String msg)
    {
        String newMsg = msg + L" at line " + std::to_wstring(line);
        return std::runtime_error(newMsg.tostr());
    }

}
}
