#include "serialize/json/jsonserializer.hpp"
#include <gtest/gtest.h>
#include <sstream>

using namespace testing;
using namespace DekaDataLibrary;
using namespace DekaDataLibrary::Serialize::JSON;

class TestJSONSerialize : public Test {
protected:
    JSONSerializer serializer;
    std::basic_ostringstream<wchar_t>* stream;

    // Test interface
protected:
    void SetUp() override;
    void TearDown() override;
};

void TestJSONSerialize::SetUp()
{
    stream = new std::basic_ostringstream<wchar_t>();
    serializer = JSONSerializer();
    serializer.setOutputStream(stream);
}

void TestJSONSerialize::TearDown()
{
    delete stream;
    stream = nullptr;
}

TEST_F(TestJSONSerialize, failIfNoStream)
{
    Object obj;
    obj["test"] = String("data");
    serializer.setOutputStream(nullptr);
    ASSERT_FALSE(serializer.serialize(obj));
}

TEST_F(TestJSONSerialize, serializeInt)
{
    Value obj = 150;
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"150");
}

TEST_F(TestJSONSerialize, serializeFloat)
{
    Value obj = 150.54;
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"150.54");
}

TEST_F(TestJSONSerialize, serializeBool)
{
    Value obj = true;
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"true");
}

TEST_F(TestJSONSerialize, serializeNull)
{
    Value obj = nullptr;
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"null");
}

TEST_F(TestJSONSerialize, serializeString)
{
    Value obj = L"big string";
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"\"big string\"");
}

TEST_F(TestJSONSerialize, serializeArray)
{

    Array arr;
    arr.push_back(5);
    arr.push_back(5.6);
    arr.push_back(nullptr);

    Value obj = arr;
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"[ 5, 5.6, null]");
}

TEST_F(TestJSONSerialize, serializeObject)
{

    Object o;
    o["test"] = true;
    o["att"] = 5;
    o["type"] = nullptr;

    Value obj = o;
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"{ \"att\" : 5, \"test\" : true, \"type\" : null}");
}

TEST_F(TestJSONSerialize, serializeEscapeChars)
{
    Value obj = "\n";
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"\"\\\n\"");
}

TEST_F(TestJSONSerialize, floatingPointPrecision)
{
    Value obj = 3.15169;
    serializer.setFloatPointPrecision(3);
    ASSERT_TRUE(serializer.serialize(obj));
    auto result = stream->str();

    ASSERT_EQ(result, L"3.152");
}
