#ifndef DDL_PROPERTIESSERIALIZER_HPP
#define DDL_PROPERTIESSERIALIZER_HPP

#include "../iserializer.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Serialize {
namespace Property {

    class DEKADATALIBRARY_EXPORT PropertiesSerializer : public ISerializer {
    private:
        String status;

    public:
        PropertiesSerializer();

        // ISerializer interface
    public:
        bool serialize(const Value& val) override;
        String getStatusMsg() const override;
        ValueTypes getSupportedTypes() const override;
    };

}
}

#endif // DDL_PROPERTIESSERIALIZER_HPP
