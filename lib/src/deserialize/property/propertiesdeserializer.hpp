#ifndef DDL_PROPERTIESDESERIALIZER_HPP
#define DDL_PROPERTIESDESERIALIZER_HPP

#include "../ideserializer.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize {
namespace Property {

    class DEKADATALIBRARY_EXPORT PropertiesDeserializer : public IDeserializer {
    private:
        String status;

        enum class ProcessLineResult {
            Error,
            Ok,
            Ignore
        };

    public:
        PropertiesDeserializer();

        // IDeserializer interface
    public:
        bool deserialize(Value& valueToFill) override;
        String getStatusMsg() const override;
        ValueTypes getSupportedTypes() const override;

    private:
        ProcessLineResult processLine(const String& line, String& key, String& value);
    };

}
}

#endif // DDL_PROPERTIESDESERIALIZER_HPP
