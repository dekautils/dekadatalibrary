#ifndef DDL_JSONPARSECONTEXT_HPP
#define DDL_JSONPARSECONTEXT_HPP

#include "../../ideserializer.hpp"
#include "dekadatalibrary_export.hpp"
#include "token.hpp"
#include <memory>
#include <stack>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class JSONParseContext;
    using JSONParseContextPtr = std::shared_ptr<JSONParseContext>;

    class DEKADATALIBRARY_NO_EXPORT JSONParseContext {
    private:
        DeserializeInputStream* stream;
        size_t line;

        std::stack<Token> tokenStack;

    public:
        JSONParseContext(DeserializeInputStream* new_stream);

        Token readNextToken();
        Token peekNextToken();

        size_t getLine() const;

        bool atEOF();

    private:
        void skipWhitespace();

        Token readBool();
        Token readNull();
        Token readNumber();
        Token readString();
        String::WChar readEscapeChar();

        std::runtime_error getRuntime(String msg);
    };

}
}

#endif // DDL_JSONPARSECONTEXT_HPP
