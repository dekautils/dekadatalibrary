#ifndef DDL_MEMBERS_HPP
#define DDL_MEMBERS_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT Members : public AbstractJSONExpression {
    public:
        Members() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}

#endif // DDL_MEMBERS_HPP
