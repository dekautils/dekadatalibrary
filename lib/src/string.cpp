#include "string.hpp"
#include <algorithm>
#include <cwctype>

namespace DekaDataLibrary {

String::String()
    : String(L"")
{
}

String::String(std::string str)
    : std::basic_string<wchar_t>(str.begin(), str.end())
{
}

String::String(std::wstring str)
    : std::basic_string<wchar_t>(str)
{
}

String::String(const char* str)
    : String(std::string(str))
{
}

String::String(const wchar_t* str)
    : String(std::wstring(str))
{
}

String String::simplified() const
{
    StringType str = *this;

    auto isspaceFunc = [](const WChar& ch) { return iswspace(ch); };
    auto firstNonSpace = std::find_if_not(str.begin(), str.end(), isspaceFunc);
    str.erase(str.begin(), firstNonSpace);

    auto reversed_str = StringType(str.rbegin(), str.rend());
    firstNonSpace = std::find_if_not(reversed_str.begin(), reversed_str.end(), isspaceFunc);
    reversed_str.erase(reversed_str.begin(), firstNonSpace);

    str = StringType(reversed_str.rbegin(), reversed_str.rend());

    if (str.size() == 0)
        return str;

    for (size_t i = 0; i < str.size() - 1; i++) {
        WChar ch0 = str[i];
        WChar ch1 = str[i + 1];

        if (isspaceFunc(ch0) && isspaceFunc(ch1)) {
            str.erase(i + 1, 1);
            i--; //Check again
        }
    }

    return str;
}

void String::replaceAll(String original, String target)
{
    auto pos = find(original, 0);

    while (pos != String::StringType::npos) {
        replace(pos, original.size(), target);
        pos = find(original, pos + target.size());
    }
}

std::string String::tostr() const
{
    return std::string(begin(), end());
}

std::wstring String::towstr() const
{
    return *this;
}

String::operator std::string() const
{
    return tostr();
}

}
