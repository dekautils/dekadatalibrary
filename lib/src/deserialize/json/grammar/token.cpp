#include "deserialize/json/grammar/token.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    TokenTypes Token::getType() const
    {
        return type;
    }

    Value Token::getVal() const
    {
        return val;
    }

    Token::Token(TokenTypes new_type, Value new_val)
        : type(new_type)
        , val(new_val)
    {
    }

}
}
