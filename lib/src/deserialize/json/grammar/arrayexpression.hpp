#ifndef DDL_ARRAYEXPRESSION_HPP
#define DDL_ARRAYEXPRESSION_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT ArrayExpression : public AbstractJSONExpression {
    public:
        ArrayExpression() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}

#endif // DDL_ARRAYEXPRESSION_HPP
