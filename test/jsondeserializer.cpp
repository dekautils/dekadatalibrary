#include "deserialize/json/jsondeserializer.hpp"
#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <list>
#include <sstream>

using namespace DekaDataLibrary;
using namespace DekaDataLibrary::Deserialize::JSON;
using namespace testing;

class TestJSONDeserializer : public Test {
protected:
    JSONDeserializer deserializer;
    std::basic_istringstream<String::WChar> stream;

    // Test interface
protected:
    void SetUp() override;
    void TearDown() override;

    void setDeserializeStr(String str);
};

void TestJSONDeserializer::SetUp()
{
    deserializer = JSONDeserializer();
    deserializer.setInputStream(nullptr);
}

void TestJSONDeserializer::TearDown()
{
    deserializer.setInputStream(nullptr);
}

void TestJSONDeserializer::setDeserializeStr(String str)
{
    stream = std::basic_istringstream<String::WChar>(str);
    deserializer.setInputStream(&stream);
}

TEST_F(TestJSONDeserializer, ObjectDeserializeValid)
{
    String testStr = L"{ "
                     "    \"object\": {}, \n"
                     "    \"array\": [0.5, null, \"str\"], \n"
                     "    \"bool\": true \n"
                     "}";

    setDeserializeStr(testStr);

    Value val;
    ASSERT_TRUE(deserializer.deserialize(val));

    ASSERT_TRUE(val.isObject());
    Object obj = val.toObject();

    ASSERT_EQ(obj.size(), 3);

    ASSERT_NE(obj.find("object"), obj.end());
    ASSERT_NE(obj.find("array"), obj.end());
    ASSERT_NE(obj.find("bool"), obj.end());

    ASSERT_TRUE(obj["object"].isObject());
    {
        auto o = obj["object"].toObject();
        ASSERT_EQ(o.size(), 0);
    }

    ASSERT_TRUE(obj["array"].isArray());
    {
        auto arr = obj["array"].toArray();
        ASSERT_EQ(arr.size(), 3);
        {
            auto v = arr[0];
            ASSERT_TRUE(v.isFloat());
            ASSERT_EQ(v.toFloat(), 0.5);
        }
        {
            auto v = arr[1];
            ASSERT_TRUE(v.isNull());
        }
        {
            auto v = arr[2];
            ASSERT_TRUE(v.isString());
            ASSERT_EQ(v.toString(), L"str");
        }
    }

    ASSERT_TRUE(obj["bool"].isBool());
    ASSERT_EQ(obj["bool"].toBool(), true);
}
