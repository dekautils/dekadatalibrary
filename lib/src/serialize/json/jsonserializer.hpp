#ifndef DDL_JSONSERIALIZER_HPP
#define DDL_JSONSERIALIZER_HPP

#include "../iserializer.hpp"
#include "dekadatalibrary_export.hpp"
#include <sstream>

namespace DekaDataLibrary::Serialize {
namespace JSON {

    class DEKADATALIBRARY_EXPORT JSONSerializer : public ISerializer {
    private:
        String status;
        std::basic_ostringstream<String::WChar> tmp_stream;
        int floatPointPrecision;

    public:
        JSONSerializer();

        void setFloatPointPrecision(int value);

        // ISerializer interface
    public:
        bool serialize(const Value& val) override;
        String getStatusMsg() const override;
        ValueTypes getSupportedTypes() const override;
        //End of ISerializer interface

    private:
        bool serializeValue(const Value& val);
        bool serializeObject(const Object& obj);
        bool serializeArray(const Array& arr);
        bool serializeFloat(const FloatType& val);
        bool serializeString(const String& str);
    };

}
}

#endif // DDL_JSONSERIALIZER_HPP
