add_gtest(NAME string SOURCES string.cpp LIBRARIES DekaDataLibrary)

add_gtest(NAME propertiesserializer SOURCES propertiesserializer.cpp LIBRARIES DekaDataLibrary)
add_gtest(NAME propertiesdeserializer SOURCES propertiesdeserializer.cpp LIBRARIES DekaDataLibrary)

add_gtest(NAME jsonserialize SOURCES jsonserialize.cpp LIBRARIES DekaDataLibrary)
add_gtest(NAME jsonparsecontext SOURCES jsonparsecontext.cpp LIBRARIES DekaDataLibrary)
add_gtest(NAME jsondeserializer SOURCES jsondeserializer.cpp LIBRARIES DekaDataLibrary)
