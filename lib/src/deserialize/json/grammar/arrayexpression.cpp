#include "deserialize/json/grammar/arrayexpression.hpp"
#include "deserialize/json/grammar/arrayelements.hpp"
#include <cassert>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value ArrayExpression::parse(JSONParseContextPtr ctx)
    {
        auto token = ctx->readNextToken();
        if (token.getType() != TokenTypes::ArrayBegin) {
            throw std::runtime_error("Unexpected token");
        }

        token = ctx->peekNextToken();
        //If empty array -> return it
        if (token.getType() == TokenTypes::ArrayEnd) {
            ctx->readNextToken();
            //assert(ctx->readNextToken().getType() == TokenTypes::ArrayEnd);
            return Array();
        }

        ArrayElements parser;
        auto out = parser.parse(ctx);

        token = ctx->readNextToken();
        if (token.getType() != TokenTypes::ArrayEnd) {
            throw std::runtime_error("Unexpected token");
        }

        return out;
    }

}
}
