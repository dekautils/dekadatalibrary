#ifndef DDL_MEMBER_HPP
#define DDL_MEMBER_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT Member : public AbstractJSONExpression {
    public:
        Member() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}

#endif // DDL_MEMBER_HPP
