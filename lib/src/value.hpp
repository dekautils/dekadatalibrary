#ifndef DDL_VALUE_HPP
#define DDL_VALUE_HPP

#include "array.hpp"
#include "dekadatalibrary_export.hpp"
#include "object.hpp"
#include "valuetype.hpp"
#include <variant>

namespace DekaDataLibrary {

class Object;
class Array;

/**
 * @brief The Value class represents multi-type value
 */
class DEKADATALIBRARY_EXPORT Value {
private:
    ValueType type; ///< Type of internal value. See ValueType

    using ValueDataType = std::variant<Object, Array, IntType, FloatType, BoolType, String>;
    ValueDataType data; ///< Stored data

public:
    /**
     * @brief Value creates undefined Value
     */
    Value();
    /**
     * @brief Value creates Value from Object
     * @param obj object to store
     */
    Value(const Object& obj);
    /**
     * @brief Value creates Value from Array
     * @param array array to store
     */
    Value(const Array& array);
    /**
     * @brief Value creates Value from IntType value
     * @param value value to store
     */
    Value(IntType value);
    /**
     * @brief Value creates Value from FloatType value
     * @param value value to store
     */
    Value(FloatType value);
    /**
     * @brief Value creates Value from BoolType value
     * @param value value to store
     */
    Value(BoolType value);
    /**
     * @brief Value creates Value from String
     * @param value string to store
     */
    Value(String value);
    /**
     * @brief Value creates Value from string
     * @param str string to store
     */
    Value(std::string str);
    /**
     * @brief Value creates Value from string
     * @param str string to store
     */
    Value(std::wstring str);
    /**
     * @brief Value creates Value from string
     * @param str string to store
     */
    Value(const char* str);
    /**
     * @brief Value creates Value from string
     * @param str string to store
     */
    Value(const wchar_t* str);
    /**
     * @brief Value creates null Value
     */
    Value(std::nullptr_t);

    ~Value();

    operator Object() const;
    operator Array() const;
    operator IntType() const;
    operator FloatType() const;
    operator BoolType() const;
    operator String() const;
    operator std::nullptr_t() const;

    /**
     * @brief Value copy constructor
     * @param other instance to copy
     */
    Value(const Value& other);
    /**
     * @brief Value move constuctor
     * @param other instance to move
     */
    Value(Value&& other);

    Value& operator=(const Value& other);
    Value& operator=(Value&& other);

    /**
     * @brief operator == compares with other Value
     * @param right right from ==
     * @return true if type and data equal, false otherwise
     */
    bool operator==(const Value& right) const;
    /**
     * @brief operator != compares with other Value
     * @param right right from !=
     * @return negation of == operator
     */
    bool operator!=(const Value& right) const;

    /**
     * @brief getType returns type of value
     * @return type of value
     */
    ValueType getType() const;

    /**
     * @brief isObject
     * @return true if this can be converted to Object
     */
    bool isObject() const;
    /**
     * @brief isArray
     * @return true if this can be converted to Array
     */
    bool isArray() const;
    /**
     * @brief isInt
     * @return true if this can be converted to IntType
     */
    bool isInt() const;
    /**
     * @brief isFloat
     * @return true if this can be converted to FloatType
     */
    bool isFloat() const;
    /**
     * @brief isDigit
     * @return true if this can be converted to IntType or FloatType
     */
    bool isDigit() const; ///< Check float or int
    /**
     * @brief isBool
     * @return true if this can be converted to BoolType
     */
    bool isBool() const;
    /**
     * @brief isString
     * @return true if this can be converted to String
     */
    bool isString() const;
    /**
     * @brief isNull
     * @return true if this is null
     */
    bool isNull() const;
    /**
     * @brief isUndefined
     * @return true if this is undefined
     */
    bool isUndefined() const;

    /**
     * @brief toObject
     * @return Object instance if possible
     */
    Object toObject() const;
    /**
     * @brief toArray
     * @return Array instance if possible
     */
    Array toArray() const;
    /**
     * @brief toInt
     * @return IntType value if possible
     */
    IntType toInt() const;
    /**
     * @brief toFloat
     * @return FloatType value if possible
     * @remark Can convert from IntType
     */
    FloatType toFloat() const;
    /**
     * @brief toBool
     * @return BoolType value if possible
     */
    BoolType toBool() const;
    /**
     * @brief toString
     * @return string if possible
     */
    String toString() const;
    /**
     * @brief toNull
     * @return nullptr if possible
     * @remark Useless, I know
     */
    std::nullptr_t toNull() const;
};

}

#endif // DDL_VALUE_HPP
