#ifndef DDL_ISERIALIZER_HPP
#define DDL_ISERIALIZER_HPP

#include "../value.hpp"
#include "../valuetype.hpp"
#include "dekadatalibrary_export.hpp"
#include <iostream>

namespace DekaDataLibrary {
namespace Serialize {

    using SerializerOutputStream = std::basic_ostream<String::WChar>;

    /**
     * @brief The ISerializer unified interface for serializing Value to wide char streams
     */
    class DEKADATALIBRARY_EXPORT ISerializer {
    protected:
        SerializerOutputStream* stream; ///< Output stream

    public:
        ISerializer() = default;
        virtual ~ISerializer() = default;

        /**
         * @brief serialize serializes input value to output stream
         * @param val value to serialize
         * @return true if serialize successful, false otherwise
         * @remark use getStatusMsg for more information if this method returns false
         */
        virtual bool serialize(const Value& val) = 0;
        /**
         * @brief setOutputStream sets output stream
         * @param new_stream new stream to set
         */
        virtual void setOutputStream(SerializerOutputStream* new_stream);
        /**
         * @brief getStatusMsg
         * @return status(Not only error) message
         */
        virtual String getStatusMsg() const = 0;

        /**
         * @brief getSupportedTypes
         * @return list of supported types
         * @todo Need move to Format
         */
        virtual ValueTypes getSupportedTypes() const = 0;
    };

}
}

#endif // DDL_ISERIALIZER_HPP
