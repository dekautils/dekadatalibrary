#include "deserialize/json/grammar/objectexpression.hpp"
#include "deserialize/json/grammar/member.hpp"
#include "deserialize/json/grammar/members.hpp"
#include <cassert>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value ObjectExpression::parse(JSONParseContextPtr ctx)
    {
        auto token = ctx->readNextToken();
        if (token.getType() != TokenTypes::ObjectBegin) {
            throw std::runtime_error("Unexpected token");
        }

        token = ctx->peekNextToken();
        //If empty obj -> return it
        if (token.getType() == TokenTypes::ObjectEnd) {
            //assert(ctx->readNextToken().getType() == TokenTypes::ObjectEnd);
            ctx->readNextToken();
            return Object();
        }

        Members parser;
        auto out = parser.parse(ctx);

        token = ctx->readNextToken();
        if (token.getType() != TokenTypes::ObjectEnd) {
            throw std::runtime_error("Unexpected token");
        }

        return out;
    }

}
}
