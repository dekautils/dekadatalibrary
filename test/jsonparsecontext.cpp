#include "deserialize/json/grammar/jsonparsecontext.hpp"
#include <algorithm>
#include <gtest/gtest.h>
#include <iostream>
#include <list>
#include <sstream>

using namespace DekaDataLibrary;
using namespace DekaDataLibrary::Deserialize::JSON::Grammar;

TEST(TestJSONParseContext, CanReadObjectDefs)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"{  }");

    JSONParseContext ctx(&stream);

    auto token1 = ctx.readNextToken();
    auto token2 = ctx.readNextToken();

    ASSERT_EQ(token1.getType(), TokenTypes::ObjectBegin);
    ASSERT_EQ(token2.getType(), TokenTypes::ObjectEnd);
}

TEST(TestJSONParseContext, CanReadArrayDefs)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"[]");

    JSONParseContext ctx(&stream);

    auto token1 = ctx.readNextToken();
    auto token2 = ctx.readNextToken();

    ASSERT_EQ(token1.getType(), TokenTypes::ArrayBegin);
    ASSERT_EQ(token2.getType(), TokenTypes::ArrayEnd);
}

TEST(TestJSONParseContext, CanReadInteger)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"465");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Number);
    ASSERT_EQ(token.getVal(), Value(465));
}

TEST(TestJSONParseContext, CanReadSimpleFloat)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"-4.65");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Number);
    ASSERT_EQ(token.getVal(), Value(-4.65));
}

TEST(TestJSONParseContext, CanReadScientificFloat)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"4.65E-5");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Number);
    ASSERT_EQ(token.getVal(), Value(4.65E-5));
}

TEST(TestJSONParseContext, CanReadBool)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"false");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Bool);
    ASSERT_EQ(token.getVal(), Value(false));
}

TEST(TestJSONParseContext, CanReadNull)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"null");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Null);
    ASSERT_EQ(token.getVal(), Value(nullptr));
}

TEST(TestJSONParseContext, CanReadString)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L"\"null object\"");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::String);
    ASSERT_EQ(token.getVal(), Value(L"null object"));
}

TEST(TestJSONParseContext, CanReadComma)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L" ,");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Comma);
}

TEST(TestJSONParseContext, CanReadColon)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L" : ");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();

    ASSERT_EQ(token.getType(), TokenTypes::Colon);
}

TEST(TestJSONParseContext, OverAllTest)
{
    String testStr = L"{ "
                     "    \"object\": {}, \n"
                     "    \"array\": [0.5, null, \"str\"], \n"
                     "    \"bool\": true \n"
                     "}";

    using T = TokenTypes;
    std::list<TokenTypes> expectedTypes = { T::ObjectBegin,

        T::String, T::Colon, T::ObjectBegin, T::ObjectEnd, T::Comma,

        T::String, T::Colon, T::ArrayBegin, T::Number, T::Comma, T::Null, T::Comma, T::String, T::ArrayEnd, T::Comma,

        T::String, T::Colon, T::Bool,

        T::ObjectEnd };

    std::basic_istringstream<String::WChar>
        stream = std::basic_istringstream<String::WChar>(testStr);

    JSONParseContext ctx(&stream);

    std::list<TokenTypes> realTokens;

    while (!ctx.atEOF()) {
        auto token = ctx.readNextToken();
        realTokens.push_back(token.getType());
    }

    auto pair = std::mismatch(expectedTypes.begin(), expectedTypes.end(), realTokens.begin());
    auto distance = std::distance(expectedTypes.begin(), pair.first);

    if (pair.first != expectedTypes.end())
        std::cerr << "Invalid token at " << distance << std::endl;

    ASSERT_EQ(pair.first, expectedTypes.end());
}

TEST(TestJSONParseContext, CanPeek)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L" [] ");

    JSONParseContext ctx(&stream);

    auto token = ctx.peekNextToken();
    ASSERT_EQ(token.getType(), TokenTypes::ArrayBegin);

    auto getted_token = ctx.readNextToken();
    ASSERT_EQ(getted_token.getType(), token.getType());
}

TEST(TestJSONParseContext, AtEOF)
{
    std::basic_istringstream<String::WChar> stream = std::basic_istringstream<String::WChar>(L" [] ");

    JSONParseContext ctx(&stream);

    auto token = ctx.readNextToken();
    ASSERT_FALSE(ctx.atEOF());
    token = ctx.readNextToken();
    ASSERT_TRUE(ctx.atEOF());
}
