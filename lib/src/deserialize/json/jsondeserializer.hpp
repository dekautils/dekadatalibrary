#ifndef DDL_JSONDESERIALIZER_HPP
#define DDL_JSONDESERIALIZER_HPP

#include "../ideserializer.hpp"
#include "dekadatalibrary_export.hpp"

using namespace DekaDataLibrary;

namespace DekaDataLibrary::Deserialize {
namespace JSON {

    class DEKADATALIBRARY_EXPORT JSONDeserializer : public IDeserializer {
    private:
        String status;

    public:
        JSONDeserializer();

        // IDeserializer interface
    public:
        bool deserialize(Value& valueToFill) override;
        String getStatusMsg() const override;
        ValueTypes getSupportedTypes() const override;
    };

}
}

#endif // DDL_JSONDESERIALIZER_HPP
