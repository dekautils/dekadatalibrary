#ifndef DDL_ARRAY_HPP
#define DDL_ARRAY_HPP

#include "dekadatalibrary_export.hpp"
#include <vector>

namespace DekaDataLibrary {

class Value;

/**
 * @brief The Array class represent vector of values(Value) It's usage like as vector from std library
 */
class DEKADATALIBRARY_EXPORT Array : public std::vector<Value> {
};

}

#endif // DDL_ARRAY_HPP
