#include "value.hpp"
#include <stdexcept>

namespace DekaDataLibrary {

Value::Value()
    : type(ValueType::Undefined)
{
}

Value::Value(const Object& obj)
    : type(ValueType::Object)
{
    data = obj;
}

Value::Value(const Array& array)
    : type(ValueType::Array)
{
    data = array;
}

Value::Value(IntType value)
    : type(ValueType::Int)
{
    data = value;
}

Value::Value(FloatType value)
    : type(ValueType::Float)
{
    data = value;
}

Value::Value(BoolType value)
    : type(ValueType::Bool)
{
    data = value;
}

Value::Value(String value)
    : type(ValueType::String)
{
    data = value;
}

Value::Value(std::string str)
    : Value(String(str))
{
}

Value::Value(std::wstring str)
    : Value(String(str))
{
}

Value::Value(const char* str)
    : Value(String(str))
{
}

Value::Value(const wchar_t* str)
    : Value(String(str))
{
}

Value::Value(std::nullptr_t val)
    : type(ValueType::Null)
{
    (void)val;
}

Value::~Value()
{
    type = ValueType::Undefined;
}

Value::Value(const Value& other)
{
    type = other.type;
    data = other.data;
}

Value::Value(Value&& other)
{
    type = std::move(other.type);
    data = std::move(other.data);
}

Value& Value::operator=(const Value& other)
{
    type = other.type;
    data = other.data;
    return *this;
}

Value& Value::operator=(Value&& other)
{
    type = std::move(other.type);
    data = std::move(other.data);
    return *this;
}

bool Value::operator==(const Value& right) const
{
    static auto compareFloatFunc = [](const FloatType& l, const FloatType& r) -> bool {
        const FloatType deriv = 0.00001;
        return l - deriv < r && r <= l + deriv;
    };

    if (this->getType() != right.getType())
        return false;
    else {
        switch (getType()) {
        case ValueType::Object:
            return toObject() == right.toObject();
        case ValueType::Array:
            return toArray() == right.toArray();
        case ValueType::Int:
            return toInt() == right.toInt();
        case ValueType::Float:
            return compareFloatFunc(toFloat(), right.toFloat());
        case ValueType::Bool:
            return toBool() == right.toBool();
        case ValueType::Null:
            return true; // Null can be only null
        case ValueType::String:
            return toString() == right.toString();
        case ValueType::Undefined:
            return true;
        }
    }

    return false;
}

bool Value::operator!=(const Value& right) const
{
    return !(*this == right);
}

ValueType Value::getType() const
{
    return type;
}

bool Value::isObject() const
{
    return getType() == ValueType::Object;
}

bool Value::isArray() const
{
    return getType() == ValueType::Array;
}

bool Value::isInt() const
{
    return getType() == ValueType::Int;
}

bool Value::isFloat() const
{
    return getType() == ValueType::Float;
}

bool Value::isDigit() const
{
    return isInt() || isFloat();
}

bool Value::isBool() const
{
    return getType() == ValueType::Bool;
}

bool Value::isString() const
{
    return getType() == ValueType::String;
}

bool Value::isNull() const
{
    return getType() == ValueType::Null;
}

bool Value::isUndefined() const
{
    return getType() == ValueType::Undefined;
}

Object Value::toObject() const
{
    return std::get<Object>(data);
}

Array Value::toArray() const
{
    return std::get<Array>(data);
}

IntType Value::toInt() const
{
    return std::get<IntType>(data);
}

FloatType Value::toFloat() const
{
    if (isInt())
        return static_cast<FloatType>(toInt());
    return std::get<FloatType>(data);
}

BoolType Value::toBool() const
{
    return std::get<BoolType>(data);
}

String Value::toString() const
{
    return std::get<String>(data);
}

std::nullptr_t Value::toNull() const
{
    if (!isNull())
        throw std::runtime_error("Cannot convert to null: It is not null");

    return nullptr;
}

Value::operator std::nullptr_t() const
{
    return nullptr;
}

Value::operator String() const
{
    return this->toString();
}

Value::operator BoolType() const
{
    return this->toBool();
}

Value::operator FloatType() const
{
    return this->toFloat();
}

Value::operator IntType() const
{
    return this->toInt();
}

Value::operator Array() const
{
    return this->toArray();
}

Value::operator Object() const
{
    return this->toObject();
}

}
