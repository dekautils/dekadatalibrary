#ifndef DDL_ARRAYELEMENTS_HPP
#define DDL_ARRAYELEMENTS_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT ArrayElements : public AbstractJSONExpression {
    public:
        ArrayElements() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}
#endif // DDL_ARRAYELEMENTS_HPP
