#ifndef DDL_STRING_HPP
#define DDL_STRING_HPP

#include "dekadatalibrary_export.hpp"
#include <string>

namespace DekaDataLibrary {

/**
 * @brief The String class represents string with wide chars
 */
class DEKADATALIBRARY_EXPORT String : public std::basic_string<wchar_t> {
public:
    using WChar = wchar_t; ///< Type of char
    using StringType = std::basic_string<WChar>; /// Parent class for this string

public:
    /**
     * @brief String creates empty string
     */
    String();
    /**
     * @brief String creates instance from std::string
     * @param str string to convert
     * @remark can be safely used only for latin chars, undefined behavior otherwise
     */
    String(std::string str);
    /**
     * @brief String creates instance from std::wstring
     * @param str wide string to convert
     */
    String(std::wstring str);
    /**
     * @brief String creates instance from null-terminated byte string
     * @param str null-terminated byte string to convert
     * @remark can be safely used only for latin chars, undefined behavior otherwise
     */
    String(const char* str);
    /**
     * @brief String creates instance from null-terminated wide char string
     * @param str null-terminated wide char string to convert
     */
    String(const wchar_t* str);

    /**
     * @brief operator std::string converts to std::string
     * @remark can be safely used only for latin chars, undefined behavior otherwise
     */
    operator std::string() const;

    /**
     * @brief simplified removes redundant whitespaces
     * @details removes all whitespace chars at begin, end of string. Reduces count of spaces in sequence at mid to 1.
     * @return simplified strings
     * @remark check whitespace char with iswspace()
     */
    String simplified() const;

    /**
     * @brief replaceAll replaces one string to another
     * @param original string to replace
     * @param target new string in place of original
     */
    void replaceAll(String original, String target);

    /**
     * @brief tostr convert to std::string
     * @return std::string
     * @remark can be safely used only for latin chars, undefined behavior otherwise
     */
    std::string tostr() const;
    /**
     * @brief towstr convert to std::wstring
     * @return std::wstring
     */
    std::wstring towstr() const;
};

}

#endif // DDL_STRING_HPP
