#include "deserialize/json/grammar/arrayelements.hpp"
#include "deserialize/json/grammar/arrayelement.hpp"
#include <cassert>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value ArrayElements::parse(JSONParseContextPtr ctx)
    {

        ArrayElement elementParser;
        Value el = elementParser.parse(ctx);
        assert(el.isArray());

        auto delim = ctx->peekNextToken();
        if (delim.getType() != TokenTypes::Comma) {
            return el;
        }

        ctx->readNextToken();
        //assert(ctx->readNextToken().getType() == TokenTypes::Comma);
        ArrayElements elementsParser;
        Value arrayElements = elementsParser.parse(ctx);
        if (!arrayElements.isArray()) {
            throw std::runtime_error("Unexpected output from ArrayElements::parse : Not Array");
        }

        Array arr;
        for (auto e : el.toArray()) {
            arr.push_back(e);
        }
        for (auto e : arrayElements.toArray()) {
            arr.push_back(e);
        }

        return arr;
    }

}
}
