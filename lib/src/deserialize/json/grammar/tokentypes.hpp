#ifndef DDL_TOKENTYPES_HPP
#define DDL_TOKENTYPES_HPP

#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    enum class DEKADATALIBRARY_NO_EXPORT TokenTypes {
        Undefined = 0,

        ObjectBegin,
        ObjectEnd,
        ArrayBegin,
        ArrayEnd,
        Number,
        String,
        Bool,
        Null,

        Comma,
        Colon
    };

}
}

#endif // DDL_TOKENTYPES_HPP
