#include "deserialize/json/grammar/member.hpp"
#include "deserialize/json/grammar/valueexpression.hpp"
#include <cassert>
#include <stdexcept>

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value Member::parse(JSONParseContextPtr ctx)
    {
        auto attNameToken = ctx->readNextToken();
        if (attNameToken.getType() != TokenTypes::String) {
            throw std::runtime_error("Unexpected token instead string");
        }

        auto delim = ctx->readNextToken();
        if (delim.getType() != TokenTypes::Colon) {
            throw std::runtime_error("Unexpected token instead ':'");
        }

        ValueExpression valParser;
        Value val = valParser.parse(ctx);

        Object out;
        String attName = attNameToken.getVal().toString();
        out[attName] = val;

        return out;
    }

}
}
