#include "deserialize/json/grammar/jsonexpression.hpp"
#include "deserialize/json/grammar/valueexpression.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    Value JsonExpression::parse(JSONParseContextPtr ctx)
    {
        ValueExpression exp;
        Value val = exp.parse(ctx);
        return val;
    }

}
}
