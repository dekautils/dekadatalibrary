#ifndef DDL_ABSTRACTJSONEXPRESSION_HPP
#define DDL_ABSTRACTJSONEXPRESSION_HPP

#include "../../../value.hpp"
#include "dekadatalibrary_export.hpp"
#include "jsonparsecontext.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT AbstractJSONExpression {
    public:
        AbstractJSONExpression() = default;
        virtual ~AbstractJSONExpression() = default;

        virtual Value parse(JSONParseContextPtr ctx) = 0;
    };

}
}

#endif // DDL_ABSTRACTJSONEXPRESSION_HPP
