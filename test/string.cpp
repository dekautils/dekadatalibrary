#include "string.hpp"
#include <gtest/gtest.h>

using namespace DekaDataLibrary;
using namespace testing;

class TestStringConvert : public Test {
protected:
    std::string normal;
    std::wstring wide;

    // Test interface
protected:
    void SetUp() override;
};

void TestStringConvert::SetUp()
{
    normal = "test";
    wide = L"test";
}

TEST_F(TestStringConvert, FromNormalToNormal)
{
    String str;
    ASSERT_NO_THROW(str = normal);

    ASSERT_EQ(str.tostr(), normal);
}

TEST_F(TestStringConvert, FromNormalToWide)
{
    String str;
    ASSERT_NO_THROW(str = normal);

    ASSERT_EQ(str, wide);
}

TEST_F(TestStringConvert, FromWideToWide)
{
    String str;
    ASSERT_NO_THROW(str = wide);

    ASSERT_EQ(str, wide);
}

class TestStringFind : public Test {
protected:
    String str;

    // Test interface
protected:
    void SetUp() override { str = "abcde"; }
};

TEST_F(TestStringFind, FindBegin)
{
    ASSERT_EQ(str.find('a'), 0);
}

TEST_F(TestStringFind, FindMed)
{
    ASSERT_EQ(str.find('d'), 3);
}

TEST_F(TestStringFind, FindEnd)
{
    ASSERT_EQ(str.find('e'), 4);
}

TEST(TestStringSimplified, SpacesBegin)
{
    String str = " \t\t  d";
    str = str.simplified();
    ASSERT_EQ(str.size(), 1);
    ASSERT_EQ(str, L"d");
}

TEST(TestStringSimplified, SpacesEnd)
{
    String str = "d  \t\n ";
    str = str.simplified();
    ASSERT_EQ(str.size(), 1);
    ASSERT_EQ(str, L"d");
}

TEST(TestStringSimplified, SpacesBeginAndEnd)
{
    String str = "  \t\t d  \t\n ";
    str = str.simplified();
    ASSERT_EQ(str.size(), 1);
    ASSERT_EQ(str, L"d");
}

TEST(TestStringSimplified, SpacesBeginAndMedAndEnd)
{
    String str = "  \t\t d \t\t g \t\n ";
    str = str.simplified();
    ASSERT_EQ(str.size(), 3);
    ASSERT_EQ(str, L"d g");
}

TEST(TestStringReplace, ReplaceOneChar)
{
    String str = "abdcabdca";
    str.replaceAll("a", "b");

    ASSERT_EQ(str, L"bbdcbbdcb");
}

TEST(TestStringReplace, ReplaceManyChar)
{
    String str = "a big text, very big";
    str.replaceAll("big", "small");

    ASSERT_EQ(str, L"a small text, very small");
}
