#ifndef DDL_IDESERIALIZER_HPP
#define DDL_IDESERIALIZER_HPP

#include "../value.hpp"
#include "../valuetype.hpp"
#include "dekadatalibrary_export.hpp"
#include <iostream>

namespace DekaDataLibrary {
namespace Deserialize {

    using DeserializeInputStream = std::basic_istream<String::WChar>;

    /**
     * @brief The IDeserializer unified interface for deserializing Value from wide char streams
     */
    class DEKADATALIBRARY_EXPORT IDeserializer {
    protected:
        DeserializeInputStream* stream; ///< Input stream

    public:
        IDeserializer() = default;
        virtual ~IDeserializer() = default;

        /**
         * @brief deserialize read stream and fill valueToFill
         * @param [out] valueToFill value for deserialized Value
         * @return true if deserialize successful, false otherwise
         * @remark use getStatusMsg for more information if this method returns false
         */
        virtual bool deserialize(Value& valueToFill) = 0;
        /**
         * @brief setInputStream sets input stream
         * @param new_stream stream to set
         */
        virtual void setInputStream(DeserializeInputStream* new_stream);
        /**
         * @brief getStatusMsg
         * @return status(Not only error) message
         */
        virtual String getStatusMsg() const = 0;

        /**
         * @brief getSupportedTypes
         * @return list of supported types of value
         * @todo Need move to Format
         */
        virtual ValueTypes getSupportedTypes() const = 0;
    };

}
}
#endif // DDL_IDESERIALIZER_HPP
