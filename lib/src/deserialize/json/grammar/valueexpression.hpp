#ifndef DDL_VALUEEXPRESSION_HPP
#define DDL_VALUEEXPRESSION_HPP

#include "abstractjsonexpression.hpp"
#include "dekadatalibrary_export.hpp"

namespace DekaDataLibrary::Deserialize::JSON {
namespace Grammar {

    class DEKADATALIBRARY_NO_EXPORT ValueExpression : public AbstractJSONExpression {
    public:
        ValueExpression() = default;

        // AbstractJSONExpression interface
    public:
        Value parse(JSONParseContextPtr ctx) override;
    };

}
}
#endif // DDL_VALUEEXPRESSION_HPP
